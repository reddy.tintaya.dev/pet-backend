from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from mascotas.models import UserPhone


class SignUpUserSerializer(ModelSerializer):
    password = serializers.CharField(write_only=True)
    phone_number = serializers.IntegerField(write_only=True)

    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name", "email", "password", "phone_number")

    def create(self, validated_data):
        phone = validated_data.pop("phone_number")
        instance = User.objects.create_user(
           **validated_data
        )
        UserPhone.objects.create(user=instance, phone_number=phone)
        return instance


class UserSerializer(ModelSerializer):
    phone = serializers.SerializerMethodField()

    def get_phone(self, obj):
        try:
            return obj.phone.first().phone_number
        except:
            pass
        return None

    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name", "email", "phone")