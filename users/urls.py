from django.urls import path

from users.views import SignUpViewSet, UserListViewSet

app_name = "users"

urlpatterns = [
    path("", UserListViewSet.as_view({"get": "list"}),
         name="users-list"
         ),
    path(
        "signup/",
        SignUpViewSet.as_view({"post": "create"}),
        name="sign-up"
    ),
]
