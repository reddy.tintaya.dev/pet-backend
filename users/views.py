from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from users.serializers import UserSerializer, SignUpUserSerializer


class UserListViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)


class SignUpViewSet(viewsets.ModelViewSet):
    serializer_class = SignUpUserSerializer
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
