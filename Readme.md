# Pet Backend app


## Installation
- `pip install -r requirements.txt`
- `python manage.py migrate`
- `python manage.py runserver`

## Endpoints
- `GET api/v1/pets/`
  ```python
  [
    {
        "name": "pet d1",
        "photo": null
    },
    {
        "name": "pet 2",
        "photo": "http://localhost:8001/pets/Screen_Shot_2022-04-22_at_09.19.55.png"
    }
]```
- `POST api/v1/pets/` multipart
    ```
  name: string
  photo: image
    ```
