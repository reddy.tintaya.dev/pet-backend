from django.contrib.auth.models import User
from django.db import models


class Pet(models.Model):
    name = models.CharField(max_length=255)
    birth_date = models.DateField(null=False, blank=False)
    weight = models.FloatField()
    photo = models.ImageField(upload_to="pets", null=True, blank=True)

    created_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="pets")
    is_missing = models.BooleanField(default=False)

    def __str__(self):
        return f"Pet: {self.name}"


class PetFoundRecords(models.Model):
    pet = models.ForeignKey(Pet, related_name="found_records", on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    seen_at = models.DateTimeField(null=True)


class UserPhone(models.Model):
    user = models.ForeignKey(User, related_name="phone", on_delete=models.CASCADE)
    phone_number = models.IntegerField(default=0)
