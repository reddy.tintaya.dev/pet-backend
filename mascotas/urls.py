from django.urls import path

from mascotas.views import PetViewSet, PetReportMissingViewSet, MissingPetViewSet, PetReportSeeingViewSet, \
    GetReportSeeingViewSet

app_name = "mascotas"

urlpatterns = [
    path(
        "pets/",
        PetViewSet.as_view({"get": "list", "post": "create"}),
        name="pets-list"
    ),
    path(
        "pets/missing/",
        MissingPetViewSet.as_view({"get": "list"}),
        name="pets-list-missing"
    ),
    path(
        "pets/<id>",
        PetViewSet.as_view({"delete": "destroy"}),
        name="pets-destroy"
    ),
    path(
        "pets/<id>/mark_as_found/",
        PetViewSet.as_view({"post": "mark_as_found"}),
        name="pets-mark-as-found"
    ),
    path(
        "pets/<id>/report-missing/",
        PetReportMissingViewSet.as_view({"post": "report_missing"}),
        name="report-missing"
    ),
    path(
        "pets/<id>/report-seeing/",
        PetReportSeeingViewSet.as_view({"post": "report_seeing"}),
        name="report-seeing"
    ),
    path(
        "pets/reports/",
        GetReportSeeingViewSet.as_view({"get": "list"}),
        name="get-report"
    )
]