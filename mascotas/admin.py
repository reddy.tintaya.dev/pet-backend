from mascotas.models import Pet, UserPhone, PetFoundRecords
from django.contrib import admin


@admin.register(Pet)
class PetAdmin(admin.ModelAdmin):
    pass


@admin.register(UserPhone)
class UserPhoneAdmin(admin.ModelAdmin):
    pass


@admin.register(PetFoundRecords)
class PetFoundRecordsAdmin(admin.ModelAdmin):
    pass
