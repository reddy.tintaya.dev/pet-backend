from django.utils import timezone
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from mascotas.models import Pet, PetFoundRecords
from mascotas.serializers import PetSerializer, PetReportSeeingSerializer, MissingPetSerializer


class PetViewSet(viewsets.ModelViewSet):
    queryset = Pet.objects.all()
    serializer_class = PetSerializer
    parser_classes = (MultiPartParser,)
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "id"

    def get_queryset(self):
        qs = super().get_queryset().filter(owner=self.request.user)
        if self.action == "list":
            return qs.filter(is_missing=False)
        return qs

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def mark_as_found(self, request, *args, **kwargs):
        pet = self.get_object()
        pet.is_missing = False
        pet.save()
        return Response()


class MissingPetViewSet(viewsets.ModelViewSet):
    queryset = Pet.objects.all()
    serializer_class = PetSerializer
    parser_classes = (MultiPartParser,)
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "id"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(is_missing=True)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class PetReportMissingViewSet(viewsets.ModelViewSet):
    queryset = Pet.objects.all()
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "id"

    def report_missing(self, request, *args, **kwargs):
        pet = self.get_object()
        pet.is_missing = True
        pet.save()
        return Response(status=204)


class PetReportSeeingViewSet(viewsets.ModelViewSet):
    queryset = Pet.objects.all()
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "id"

    def report_seeing(self, request, *args, **kwargs):
        pet = self.get_object()
        longitude = request.data.get("longitude")
        latitude = request.data.get("latitude")
        PetFoundRecords.objects.create(pet=pet, user=self.request.user, longitude=longitude, latitude=latitude,
                                       seen_at=timezone.now())
        phone_contact = pet.owner.phone.first().phone_number

        return Response({"phone_contact": phone_contact})


class GetReportSeeingViewSet(viewsets.ModelViewSet):
    queryset = Pet.objects.all()
    serializer_class = MissingPetSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    lookup_url_kwarg = "id"

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)
