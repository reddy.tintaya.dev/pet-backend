from datetime import datetime

from dateutil.relativedelta import relativedelta
from rest_framework import serializers

from mascotas.models import Pet, PetFoundRecords
from users.serializers import UserSerializer


class PetSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField(read_only=True)
    birth_date = serializers.DateField(write_only=True)
    owner = UserSerializer(read_only=True)

    def get_age(self, obj):
        now = datetime.now()
        time_difference = relativedelta(obj.birth_date, now)
        return abs(time_difference.years)

    class Meta:
        model = Pet
        fields = ("id", "name", "photo", "age", "weight", "photo", "owner", "birth_date")
        read_only_fields = ("id", "age")


class MissingPetSerializer(serializers.ModelSerializer):
    founds = serializers.SerializerMethodField()
    age = serializers.SerializerMethodField(read_only=True)

    def get_age(self, obj):
        now = datetime.now()
        time_difference = relativedelta(obj.birth_date, now)
        return abs(time_difference.years)


    def get_founds(self, obj):
        serializer = PetReportSeeingSerializer(obj.found_records, many=True)
        return serializer.data

    class Meta:
        model = Pet
        fields = ("id", "name", "photo", "age", "weight", "photo", "birth_date", "founds")


class PetReportSeeingSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = PetFoundRecords
        fields = ("user", "latitude", "longitude", "seen_at")
